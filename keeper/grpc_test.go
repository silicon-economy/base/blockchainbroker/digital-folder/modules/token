// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"context"
	"fmt"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

func (suite *TokenTestSuite) TestGRPCWallet() {
	tokenId := suite.SetupToken()
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	token := testableApp.TokenKeeper.GetToken(ctx, tokenId)
	suite.Require().NotNil(token)

	var req *types.QueryGetTokenRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.EmptyRequest,
			func() {
				req = &types.QueryGetTokenRequest{}
			},
			false,
		},
		{
			app.ValidRequest,
			func() {
				req = &types.QueryGetTokenRequest{Id: tokenId}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.Token(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Require().Equal("", res.Token.Id)
			}
		})
	}
}

func (suite *TokenTestSuite) TestGRPCAllToken() {
	suite.SetupToken()
	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	token := testableApp.TokenKeeper.GetAllToken(ctx)
	suite.Require().NotNil(token)

	var req *types.QueryAllTokenRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.ValidRequest,
			func() {
				req = &types.QueryAllTokenRequest{}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.TokenAll(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Nil(res)
			}
		})
	}
}

func (suite *TokenTestSuite) TestGRPCTokenHistory() {
	tokenId := suite.SetupToken()

	token := suite.keeper.GetToken(suite.ctx, tokenId)
	token.Creator = app.TokenCreator
	suite.keeper.SetToken(suite.ctx, token)

	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	tokenHistory := testableApp.TokenKeeper.GetTokenHistory(ctx, tokenId)
	suite.Require().NotNil(tokenHistory)

	var req *types.QueryGetTokenHistoryRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.EmptyRequest,
			func() {
				req = &types.QueryGetTokenHistoryRequest{}
			},
			false,
		},
		{
			app.ValidRequest,
			func() {
				req = &types.QueryGetTokenHistoryRequest{Id: tokenId}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.TokenHistory(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Nil(res.TokenHistory.History)
			}
		})
	}
}

func (suite *TokenTestSuite) TestGRPCAllTokenHistory() {
	tokenId := suite.SetupToken()

	token := suite.keeper.GetToken(suite.ctx, tokenId)
	token.Creator = app.TokenCreator
	suite.keeper.SetToken(suite.ctx, token)

	testableApp, ctx, queryClient := suite.app, suite.ctx, suite.queryClient
	tokenHistory := testableApp.TokenKeeper.GetAllTokenHistory(ctx)
	suite.Require().NotNil(tokenHistory)

	var req *types.QueryAllTokenHistoryRequest

	testCases := []struct {
		msg     string
		setup   func()
		expPass bool
	}{
		{
			app.ValidRequest,
			func() {
				req = &types.QueryAllTokenHistoryRequest{}
			},
			true,
		},
	}

	for _, tc := range testCases {
		suite.Run(fmt.Sprintf("Case %s", tc.msg), func() {
			tc.setup()
			res, err := queryClient.TokenHistoryAll(context.Background(), req)
			if tc.expPass {
				suite.NoError(err)
			} else {
				suite.Nil(res)
			}
		})
	}
}
