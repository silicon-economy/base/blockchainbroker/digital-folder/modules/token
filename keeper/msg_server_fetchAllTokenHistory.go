// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/types/query"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

func (k Keeper) FetchAllTokenHistory(goCtx context.Context, msg *types.MsgFetchAllTokenHistory) (*types.MsgFetchAllTokenHistoryResponse, error) {
	res, _ := k.TokenHistoryAll(goCtx, &types.QueryAllTokenHistoryRequest{Pagination: &query.PageRequest{}})
	return &types.MsgFetchAllTokenHistoryResponse{TokenHistory: res.TokenHistory}, nil
}
