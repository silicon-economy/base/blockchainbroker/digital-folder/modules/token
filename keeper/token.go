// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

// GetTokenCount gets the total number of token
func (k Keeper) GetTokenCount(ctx sdk.Context) int64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenCountKey))
	byteKey := types.KeyPrefix(types.TokenCountKey)
	bz := store.Get(byteKey)

	// Count doesn't exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	count, err := strconv.ParseInt(string(bz), 10, 64)
	if err != nil {
		// Panic because the count should be always formattable to int64
		panic("cannot decode count")
	}

	return count
}

// SetTokenCount sets the total number of token
func (k Keeper) SetTokenCount(ctx sdk.Context, count int64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenCountKey))
	byteKey := types.KeyPrefix(types.TokenCountKey)
	bz := []byte(strconv.FormatInt(count, 10))
	store.Set(byteKey, bz)
}

// AppendToken appends a token in the store with a new id and updates the count
func (k Keeper) AppendToken(
	ctx sdk.Context,
	creator string,
	id string,
	timestamp string,
	tokenType string,
	changeMessage string,
	segmentId string,
) string {
	// Create the token
	count := k.GetTokenCount(ctx)
	var token = types.Token{
		Creator:       creator,
		Id:            id,
		Timestamp:     timestamp,
		TokenType:     tokenType,
		ChangeMessage: changeMessage,
		Valid:         true,
		SegmentId:     segmentId,
	}

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenKey))
	key := types.KeyPrefix(types.TokenKey + token.Id)
	value := k.cdc.MustMarshal(&token)
	store.Set(key, value)

	// Update token count
	k.SetTokenCount(ctx, count+1)

	return id
}

// SetToken sets a specific token in the store
func (k Keeper) SetToken(ctx sdk.Context, token types.Token) {

	oldToken := k.GetToken(ctx, token.Id)

	k.CreateTokenHistoryEntry(ctx, oldToken)

	token.Timestamp = GetTimestamp()

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenKey))
	b := k.cdc.MustMarshal(&token)
	store.Set(types.KeyPrefix(types.TokenKey+token.Id), b)
}

// CreateTokenHistoryEntry creates an entry in the token history
func (k Keeper) CreateTokenHistoryEntry(ctx sdk.Context, oldToken types.Token) {
	tokenHistoryExists := k.HasTokenHistory(ctx, oldToken.Id)

	if tokenHistoryExists {
		tokenHistory := k.GetTokenHistory(ctx, oldToken.Id)
		updatedHistory := append(tokenHistory.History, &oldToken)
		tokenHistory.History = updatedHistory
		k.SetTokenHistory(ctx, tokenHistory)
	} else {
		tokenHistory := types.MsgCreateTokenHistory{
			Creator: oldToken.Creator,
			Id:      oldToken.Id,
			History: []*types.Token{
				&oldToken,
			},
		}
		k.AppendTokenHistory(ctx, tokenHistory)
	}
}

// GetToken returns a token from its id
func (k Keeper) GetToken(ctx sdk.Context, key string) types.Token {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenKey))
	var token types.Token
	k.cdc.MustUnmarshal(store.Get(types.KeyPrefix(types.TokenKey+key)), &token)
	return token
}

// HasToken checks if the token exists in the store
func (k Keeper) HasToken(ctx sdk.Context, id string) bool {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenKey))
	return store.Has(types.KeyPrefix(types.TokenKey + id))
}

// GetTokenOwner returns the creator of the token
func (k Keeper) GetTokenOwner(ctx sdk.Context, key string) string {
	return k.GetToken(ctx, key).Creator
}

// RemoveToken removes a token from the store
func (k Keeper) RemoveToken(ctx sdk.Context, key string) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenKey))
	store.Delete(types.KeyPrefix(types.TokenKey + key))
}

// SetTrue gets a token from the store and sets the valid flag to true
func (k Keeper) SetTrue(ctx sdk.Context, key string) {
	token := k.GetToken(ctx, key)
	token.Valid = true
	k.SetToken(ctx, token)
}

// SetFalse gets a token from the store and sets the valid flag to false
func (k Keeper) SetFalse(ctx sdk.Context, key string) {
	token := k.GetToken(ctx, key)
	token.Valid = false
	k.SetToken(ctx, token)
}

// SetTokenInformation gets a token from the store and sets the info struct of the token
func (k Keeper) SetTokenInformation(ctx sdk.Context, msg types.MsgUpdateTokenInformation) {
	token := k.GetToken(ctx, msg.TokenId)
	infoStruct := types.Info{
		Data: msg.Data,
	}
	token.Info = &infoStruct
	token.Creator = msg.Creator
	k.SetToken(ctx, token)
}

// GetAllToken returns all token
func (k Keeper) GetAllToken(ctx sdk.Context) (msgs []types.Token) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenKey))
	iterator := sdk.KVStorePrefixIterator(store, types.KeyPrefix(types.TokenKey))

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var msg types.Token
		k.cdc.MustUnmarshal(iterator.Value(), &msg)
		msgs = append(msgs, msg)
	}

	return
}
