// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/types/query"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

func (k Keeper) FetchAllToken(goCtx context.Context, msg *types.MsgFetchAllToken) (*types.MsgFetchAllTokenResponse, error) {
	res, _ := k.TokenAll(goCtx, &types.QueryAllTokenRequest{Pagination: &query.PageRequest{}})
	return &types.MsgFetchAllTokenResponse{Token: res.Token}, nil
}
