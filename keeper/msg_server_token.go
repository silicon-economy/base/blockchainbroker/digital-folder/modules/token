// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"
	"fmt"

	sdk "github.com/cosmos/cosmos-sdk/types"
	sdkErrors "github.com/cosmos/cosmos-sdk/types/errors"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

func (k Keeper) CreateToken(goCtx context.Context, msg *types.MsgCreateToken) (*types.MsgIdResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)
	id := k.AppendToken(
		ctx,
		msg.Creator,
		msg.Id,
		GetTimestamp(),
		msg.TokenType,
		msg.ChangeMessage,
		msg.SegmentId,
	)

	return &types.MsgIdResponse{
		Id: id,
	}, nil
}

func (k Keeper) UpdateToken(goCtx context.Context, msg *types.MsgUpdateToken) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	// Checks that the element exists
	if !k.HasToken(ctx, msg.Id) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.Id))
	}

	oldToken := k.GetToken(ctx, msg.Id)

	var token = types.Token{
		Creator:       msg.Creator,
		Id:            msg.Id,
		SegmentId:     oldToken.SegmentId,
		Timestamp:     GetTimestamp(),
		TokenType:     msg.TokenType,
		ChangeMessage: msg.ChangeMessage,
		Info:          oldToken.Info,
		Valid:         oldToken.Valid,
	}

	k.SetToken(ctx, token)

	return &types.MsgEmptyResponse{}, nil
}

func (k Keeper) ActivateToken(goCtx context.Context, msg *types.MsgActivateToken) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	if !k.HasToken(ctx, msg.Id) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.Id))
	}

	k.SetTrue(ctx, msg.Id)

	return &types.MsgEmptyResponse{}, nil
}

func (k Keeper) DeactivateToken(goCtx context.Context, msg *types.MsgDeactivateToken) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	if !k.HasToken(ctx, msg.Id) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.Id))
	}

	k.SetFalse(ctx, msg.Id)

	return &types.MsgEmptyResponse{}, nil
}

func (k Keeper) UpdateTokenInformation(goCtx context.Context, msg *types.MsgUpdateTokenInformation) (*types.MsgEmptyResponse, error) {
	ctx := sdk.UnwrapSDKContext(goCtx)

	if !k.HasToken(ctx, msg.TokenId) {
		return nil, sdkErrors.Wrap(sdkErrors.ErrKeyNotFound, fmt.Sprintf("key %s doesn't exist", msg.TokenId))
	}

	k.SetTokenInformation(ctx, *msg)

	return &types.MsgEmptyResponse{}, nil
}
