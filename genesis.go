// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package Token

import (
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

// InitGenesis initializes the capability module's state from a provided genesis state.
func InitGenesis(ctx sdk.Context, k keeper.Keeper, genState types.GenesisState) {
	k.InitGenesis(ctx, genState)
}

// ExportGenesis returns the capability module's exported genesis.
func ExportGenesis(ctx sdk.Context, k keeper.Keeper) *types.GenesisState {
	genesis := types.DefaultGenesis()

	// this line is used by starport scaffolding # genesis/module/export
	// Get all tokenHistory
	tokenHistoryList := k.GetAllTokenHistory(ctx)
	for _, elem := range tokenHistoryList {
		elem := elem
		genesis.TokenHistoryList = append(genesis.TokenHistoryList, &elem)
	}

	// Get all token
	tokenList := k.GetAllToken(ctx)
	for _, elem := range tokenList {
		elem := elem
		genesis.TokenList = append(genesis.TokenList, &elem)
	}

	return genesis
}
