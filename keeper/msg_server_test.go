// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

func (suite *TokenTestSuite) TestMsgCreateToken() {
	tests := []struct {
		name    string
		msg     *types.MsgCreateToken
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			msg:     &types.MsgCreateToken{Creator: app.CreatorA, Id: app.TokenId1, ChangeMessage: app.Changed},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		id, errCreate := suite.msgServer.CreateToken(suite.goCtx, tt.msg)
		got := suite.keeper.GetToken(suite.ctx, id.Id)
		if tt.wantErr {
			suite.Error(errCreate, "Should have thrown an Error")
		} else {
			suite.Require().Equal(app.Changed, got.ChangeMessage)
		}
	}
}

func (suite *TokenTestSuite) TestMsgUpdateToken() {
	token, _ := suite.msgServer.CreateToken(suite.goCtx, &types.MsgCreateToken{Creator: app.CreatorA, Id: app.TokenId1, ChangeMessage: app.Changed})
	tokenId := token.Id
	newChgMsg := "NEW_CHANGE_MESSAGE"

	tests := []struct {
		name    string
		msg     *types.MsgUpdateToken
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			msg:     &types.MsgUpdateToken{Creator: app.CreatorA, Id: tokenId, ChangeMessage: newChgMsg},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		_, errCreate := suite.msgServer.UpdateToken(suite.goCtx, tt.msg)
		got := suite.keeper.GetToken(suite.ctx, tokenId)
		if tt.wantErr {
			suite.Error(errCreate, "Should have thrown an Error")
		} else {
			suite.Require().Equal(newChgMsg, got.ChangeMessage)
		}
	}
}

func (suite *TokenTestSuite) TestMsgActivateToken() {
	token, _ := suite.msgServer.CreateToken(suite.goCtx, &types.MsgCreateToken{Creator: app.CreatorA, Id: app.TokenId1})
	tokenId := token.Id

	tests := []struct {
		name          string
		msgDeactivate *types.MsgDeactivateToken
		msgActivate   *types.MsgActivateToken
		wantErr       bool
	}{
		{
			name:          app.SuccessfulTx,
			msgDeactivate: &types.MsgDeactivateToken{Creator: app.CreatorA, Id: tokenId},
			msgActivate:   &types.MsgActivateToken{Creator: app.CreatorA, Id: tokenId},
			wantErr:       false,
		},
	}

	for _, tt := range tests {
		// Initially Token should be Valid
		suite.Require().True(suite.keeper.GetToken(suite.ctx, tokenId).Valid)

		// Set to Invalid
		_, errDeactivate := suite.msgServer.DeactivateToken(suite.goCtx, tt.msgDeactivate)
		if tt.wantErr {
			suite.Error(errDeactivate, "Should have thrown an Error")
		} else {
			suite.Require().False(suite.keeper.GetToken(suite.ctx, tokenId).Valid)
		}

		// Set back to Valid
		_, errActivate := suite.msgServer.ActivateToken(suite.goCtx, tt.msgActivate)
		if tt.wantErr {
			suite.Error(errActivate, "Should have thrown an Error")
		} else {
			suite.Require().True(suite.keeper.GetToken(suite.ctx, tokenId).Valid)
		}
	}
}

func (suite *TokenTestSuite) TestMsgUpdateTokenInformation() {
	token, _ := suite.msgServer.CreateToken(suite.goCtx, &types.MsgCreateToken{Creator: app.CreatorA, Id: app.TokenId1, ChangeMessage: app.Changed})
	tokenId := token.Id
	newData := "NEW_DATA"

	tests := []struct {
		name    string
		msg     *types.MsgUpdateTokenInformation
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			msg:     &types.MsgUpdateTokenInformation{Creator: app.CreatorA, TokenId: tokenId, Data: newData},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		_, errCreate := suite.msgServer.UpdateTokenInformation(suite.goCtx, tt.msg)
		got := suite.keeper.GetToken(suite.ctx, tokenId)

		if tt.wantErr {
			suite.Error(errCreate, "Should have thrown an Error")
		} else {
			suite.Require().Equal(newData, got.Info.Data)
		}
	}
}
