// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

func (k Keeper) FetchTokenHistory(goCtx context.Context, msg *types.MsgFetchTokenHistory) (*types.MsgFetchTokenHistoryResponse, error) {
	res, _ := k.TokenHistory(goCtx, &types.QueryGetTokenHistoryRequest{Id: msg.Id})
	return &types.MsgFetchTokenHistoryResponse{TokenHistory: res.TokenHistory}, nil
}
