// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

func (k Keeper) FetchToken(goCtx context.Context, msg *types.MsgFetchToken) (*types.MsgFetchTokenResponse, error) {
	res, _ := k.Token(goCtx, &types.QueryGetTokenRequest{Id: msg.Id})
	return &types.MsgFetchTokenResponse{Token: res.Token}, nil
}
