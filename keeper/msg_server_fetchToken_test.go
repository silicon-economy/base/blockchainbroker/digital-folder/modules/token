// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"context"
	"reflect"
	"testing"

	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

func Test_msgServer_FetchToken(t *testing.T) {
	newKeeper, ctx := setupKeeper(t)
	goCtx := sdk.WrapSDKContext(ctx)

	type fields struct {
		Keeper keeper.Keeper
	}

	type args struct {
		goCtx context.Context
		msg   *types.MsgFetchToken
	}

	tests := []struct {
		name    string
		fields  fields
		args    args
		want    *types.MsgFetchTokenResponse
		wantErr bool
	}{
		{
			name:    app.SuccessfulTx,
			fields:  fields{Keeper: *newKeeper},
			args:    args{goCtx: goCtx, msg: &types.MsgFetchToken{Creator: app.CreatorA}},
			want:    &types.MsgFetchTokenResponse{Token: &types.Token{}},
			wantErr: false,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			msgServer := keeper.NewMsgServerImpl(*newKeeper)
			got, err := msgServer.FetchToken(tt.args.goCtx, tt.args.msg)

			if (err != nil) != tt.wantErr {
				t.Errorf("msgServer.FetchToken() error = %v, wantErr %v", err, tt.wantErr)
				return
			}

			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("msgServer.FetchToken() = %v, want %v", got, tt.want)
			}
		})
	}
}
