// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"context"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/cosmos/cosmos-sdk/types/query"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

func (k Keeper) TokenHistoryAll(c context.Context, req *types.QueryAllTokenHistoryRequest) (*types.QueryAllTokenHistoryResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	var tokenHistories []*types.TokenHistory
	ctx := sdk.UnwrapSDKContext(c)

	store := ctx.KVStore(k.storeKey)
	tokenHistoryStore := prefix.NewStore(store, types.KeyPrefix(types.TokenHistoryKey))

	pageRes, err := query.Paginate(tokenHistoryStore, req.Pagination, func(key []byte, value []byte) error {
		var tokenHistory types.TokenHistory
		if err := k.cdc.Unmarshal(value, &tokenHistory); err != nil {
			return err
		}

		tokenHistories = append(tokenHistories, &tokenHistory)
		return nil
	})

	if err != nil {
		return nil, status.Error(codes.Internal, err.Error())
	}

	return &types.QueryAllTokenHistoryResponse{TokenHistory: tokenHistories, Pagination: pageRes}, nil
}

func (k Keeper) TokenHistory(c context.Context, req *types.QueryGetTokenHistoryRequest) (*types.QueryGetTokenHistoryResponse, error) {
	if req == nil {
		return nil, status.Error(codes.InvalidArgument, "invalid request")
	}

	var tokenHistory types.TokenHistory
	ctx := sdk.UnwrapSDKContext(c)

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryKey))
	k.cdc.MustUnmarshal(store.Get(types.KeyPrefix(types.TokenHistoryKey+req.Id)), &tokenHistory)

	return &types.QueryGetTokenHistoryResponse{TokenHistory: &tokenHistory}, nil
}
