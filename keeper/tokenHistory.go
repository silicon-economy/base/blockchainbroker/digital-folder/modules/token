// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"strconv"

	"github.com/cosmos/cosmos-sdk/store/prefix"
	sdk "github.com/cosmos/cosmos-sdk/types"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

// GetTokenHistoryCount gets the total number of tokenHistory
func (k Keeper) GetTokenHistoryCount(ctx sdk.Context) int64 {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryCountKey))
	byteKey := types.KeyPrefix(types.TokenHistoryCountKey)
	bz := store.Get(byteKey)

	// Count doesn't exist: no element
	if bz == nil {
		return 0
	}

	// Parse bytes
	count, err := strconv.ParseInt(string(bz), 10, 64)
	if err != nil {
		// Panic because the count should be always formattable to int64
		panic("cannot decode count")
	}

	return count
}

// SetTokenHistoryCount sets the total number of tokenHistory
func (k Keeper) SetTokenHistoryCount(ctx sdk.Context, count int64) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryCountKey))
	byteKey := types.KeyPrefix(types.TokenHistoryCountKey)
	bz := []byte(strconv.FormatInt(count, 10))
	store.Set(byteKey, bz)
}

// AppendTokenHistory appends a tokenHistory in the store with a new id and updates the count
func (k Keeper) AppendTokenHistory(
	ctx sdk.Context,
	msg types.MsgCreateTokenHistory,
) string {
	// Create the tokenHistory
	count := k.GetTokenHistoryCount(ctx)
	// _ := strconv.FormatInt(count, 10)
	var tokenHistory = types.TokenHistory{
		Creator: msg.Creator,
		Id:      msg.Id,
		History: msg.History,
	}

	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryKey))
	key := types.KeyPrefix(types.TokenHistoryKey + tokenHistory.Id)
	value := k.cdc.MustMarshal(&tokenHistory)
	store.Set(key, value)

	// Update tokenHistory count
	k.SetTokenHistoryCount(ctx, count+1)

	return msg.Id
}

// SetTokenHistory sets a specific tokenHistory in the store
func (k Keeper) SetTokenHistory(ctx sdk.Context, tokenHistory types.TokenHistory) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryKey))
	b := k.cdc.MustMarshal(&tokenHistory)
	store.Set(types.KeyPrefix(types.TokenHistoryKey+tokenHistory.Id), b)
}

// GetTokenHistory returns a tokenHistory from its id
func (k Keeper) GetTokenHistory(ctx sdk.Context, key string) types.TokenHistory {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryKey))
	var tokenHistory types.TokenHistory
	k.cdc.MustUnmarshal(store.Get(types.KeyPrefix(types.TokenHistoryKey+key)), &tokenHistory)
	return tokenHistory
}

// HasTokenHistory checks if the tokenHistory exists in the store
func (k Keeper) HasTokenHistory(ctx sdk.Context, id string) bool {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryKey))
	return store.Has(types.KeyPrefix(types.TokenHistoryKey + id))
}

// GetTokenHistoryOwner returns the creator of the tokenHistory
func (k Keeper) GetTokenHistoryOwner(ctx sdk.Context, key string) string {
	return k.GetTokenHistory(ctx, key).Creator
}

// RemoveTokenHistory removes a tokenHistory from the store
func (k Keeper) RemoveTokenHistory(ctx sdk.Context, key string) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryKey))
	store.Delete(types.KeyPrefix(types.TokenHistoryKey + key))
}

// GetAllTokenHistory returns all tokenHistory
func (k Keeper) GetAllTokenHistory(ctx sdk.Context) (msgs []types.TokenHistory) {
	store := prefix.NewStore(ctx.KVStore(k.storeKey), types.KeyPrefix(types.TokenHistoryKey))
	iterator := sdk.KVStorePrefixIterator(store, types.KeyPrefix(types.TokenHistoryKey))

	defer iterator.Close()

	for ; iterator.Valid(); iterator.Next() {
		var msg types.TokenHistory
		k.cdc.MustUnmarshal(iterator.Value(), &msg)
		msgs = append(msgs, msg)
	}

	return
}
