// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"context"
	"testing"

	"github.com/cosmos/cosmos-sdk/baseapp"
	"github.com/cosmos/cosmos-sdk/codec"
	codecTypes "github.com/cosmos/cosmos-sdk/codec/types"
	"github.com/cosmos/cosmos-sdk/store"
	storeTypes "github.com/cosmos/cosmos-sdk/store/types"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/stretchr/testify/require"
	"github.com/stretchr/testify/suite"
	"github.com/tendermint/tendermint/libs/log"
	tendermintTypes "github.com/tendermint/tendermint/proto/tendermint/types"
	tendermintTmDb "github.com/tendermint/tm-db"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	authorizationKeeper "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/keeper"
	authorizationTypes "git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/authorization/types"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/keeper"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

type TokenTestSuite struct {
	suite.Suite
	keeper              keeper.Keeper
	app                 *app.App
	ctx                 sdk.Context
	goCtx               context.Context
	queryClient         types.QueryClient
	addr1               sdk.AccAddress
	addr2               sdk.AccAddress
	msgServer           types.MsgServer
	authorizationKeeper authorizationKeeper.Keeper
}

func setupKeeper(t testing.TB) (*keeper.Keeper, sdk.Context) {
	storeKey := sdk.NewKVStoreKey(types.StoreKey)
	memStoreKey := storeTypes.NewMemoryStoreKey(types.MemStoreKey)

	db := tendermintTmDb.NewMemDB()
	stateStore := store.NewCommitMultiStore(db)
	stateStore.MountStoreWithDB(storeKey, sdk.StoreTypeIAVL, db)
	stateStore.MountStoreWithDB(memStoreKey, sdk.StoreTypeMemory, nil)
	require.NoError(t, stateStore.LoadLatestVersion())

	registry := codecTypes.NewInterfaceRegistry()
	newKeeper := keeper.NewKeeper(
		codec.NewProtoCodec(registry),
		storeKey,
		memStoreKey,
	)

	ctx := sdk.NewContext(stateStore, tendermintTypes.Header{}, false, log.NewNopLogger())

	return newKeeper, ctx
}

func (suite *TokenTestSuite) SetupTest() {
	testableApp, ctx := app.CreateTestInput()

	suite.keeper = testableApp.TokenKeeper
	suite.authorizationKeeper = testableApp.AuthorizationKeeper
	suite.app = testableApp
	suite.ctx = ctx
	suite.goCtx = sdk.WrapSDKContext(suite.ctx)

	creator1, _ := sdk.AccAddressFromBech32(app.CreatorA)
	creator2, _ := sdk.AccAddressFromBech32(app.CreatorB)

	suite.addr1 = creator1
	suite.addr2 = creator2

	querier := keeper.Querier{Keeper: testableApp.TokenKeeper}
	queryHelper := baseapp.NewQueryServerTestHelper(ctx, testableApp.InterfaceRegistry())
	types.RegisterQueryServer(queryHelper, querier)

	suite.queryClient = types.NewQueryClient(queryHelper)
	suite.msgServer = keeper.NewMsgServerImpl(suite.keeper)

	suite.authorizationKeeper.SetConfiguration(suite.ctx, authorizationTypes.Configuration{
		Creator:         app.CreatorA,
		Id:              0,
		PermissionCheck: true,
	})
}

func (suite *TokenTestSuite) SetupToken() string {
	msgCreateToken := types.MsgCreateToken{
		Creator:       suite.addr1.String(),
		Id:            app.TokenId1,
		Timestamp:     app.Timestamp,
		TokenType:     app.TokenType,
		ChangeMessage: app.Changed,
	}

	id := suite.keeper.AppendToken(
		suite.ctx,
		msgCreateToken.Creator,
		msgCreateToken.Id,
		msgCreateToken.Timestamp,
		msgCreateToken.TokenType,
		msgCreateToken.ChangeMessage,
		msgCreateToken.SegmentId)
	return id
}
