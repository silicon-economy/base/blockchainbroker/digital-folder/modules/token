// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper

import (
	"fmt"
	"time"

	"github.com/cosmos/cosmos-sdk/codec"
	sdk "github.com/cosmos/cosmos-sdk/types"
	"github.com/tendermint/tendermint/libs/log"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

type (
	Keeper struct {
		cdc      codec.Codec
		storeKey sdk.StoreKey
		memKey   sdk.StoreKey
	}
)

func NewKeeper(cdc codec.Codec, storeKey, memKey sdk.StoreKey) *Keeper {
	return &Keeper{
		cdc:      cdc,
		storeKey: storeKey,
		memKey:   memKey,
	}
}

func (k Keeper) Logger(ctx sdk.Context) log.Logger {
	return ctx.Logger().With("module", fmt.Sprintf("x/%s", types.ModuleName))
}

func GetTimestamp() string {
	return time.Now().UTC().Format(time.RFC3339)
}

// InitGenesis initializes the capability module's state from a provided genesis state.
func (k Keeper) InitGenesis(ctx sdk.Context, genState types.GenesisState) {
	// this line is used by starport scaffolding # genesis/module/init
	// Set all the tokenHistory
	for _, elem := range genState.TokenHistoryList {
		k.SetTokenHistory(ctx, *elem)
	}

	// Set tokenHistory count
	k.SetTokenHistoryCount(ctx, int64(len(genState.TokenHistoryList)))

	// Set all the token
	for _, elem := range genState.TokenList {
		k.SetToken(ctx, *elem)
	}

	// Set token count
	k.SetTokenCount(ctx, int64(len(genState.TokenList)))
}

// RevertToGenesis reverts to genesis state
func (k Keeper) RevertToGenesis(ctx sdk.Context) {
	kvStore := ctx.KVStore(k.storeKey)
	it := kvStore.Iterator(nil, nil)
	defer it.Close()

	for ; it.Valid(); it.Next() {
		kvStore.Delete(it.Key())
	}

	k.InitGenesis(ctx, *types.DefaultGenesis())
}
