// Copyright Open Logistics Foundation
//
// Licensed under the Open Logistics Foundation License 1.3.
// For details on the licensing terms, see the LICENSE file.
// SPDX-License-Identifier: OLFL-1.3

package keeper_test

import (
	"testing"

	"github.com/stretchr/testify/suite"

	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/app"
	"git.openlogisticsfoundation.org/silicon-economy/base/blockchainbroker/digital-folder/TokenManager/x/token/types"
)

func TestTokenTestSuite(t *testing.T) {
	suite.Run(t, new(TokenTestSuite))
}

func (suite *TokenTestSuite) TestCreateToken() {
	addr := []string{suite.addr1.String(), suite.addr2.String()}
	tokenIds := []string{app.TokenId1, app.TokenId2}
	setup := []types.MsgCreateToken{
		{
			Creator:       suite.addr1.String(),
			Id:            app.TokenId1,
			Timestamp:     app.Timestamp,
			TokenType:     app.TokenType,
			ChangeMessage: app.Changed,
			SegmentId:     app.SegmentId1,
		},
		{
			Creator:       suite.addr2.String(),
			Id:            app.TokenId2,
			Timestamp:     app.Timestamp,
			TokenType:     app.TokenType,
			ChangeMessage: app.Changed,
			SegmentId:     app.SegmentId1,
		},
	}

	for index, element := range setup {
		id := suite.keeper.AppendToken(suite.ctx, element.Creator, element.Id, element.Timestamp, element.TokenType, element.ChangeMessage, element.SegmentId)
		out := suite.keeper.GetToken(suite.ctx, id)
		suite.Require().Equal(addr[index], out.Creator)
		suite.Require().Equal(tokenIds[index], out.Id)
		suite.Require().Equal(app.Timestamp, out.Timestamp)
		suite.Require().Equal(app.TokenType, out.TokenType)
		suite.Require().Equal(app.Changed, out.ChangeMessage)
		suite.Require().True(suite.keeper.HasToken(suite.ctx, tokenIds[index]))
		suite.Require().Equal(suite.keeper.GetTokenOwner(suite.ctx, tokenIds[index]), out.Creator)
		suite.Require().Equal(index+1, len(suite.keeper.GetAllToken(suite.ctx)))
	}
}

func (suite *TokenTestSuite) TestSetToken() {
	setup := []types.MsgUpdateToken{
		{
			Creator:       suite.addr1.String(),
			Timestamp:     app.Timestamp,
			TokenType:     app.TokenType,
			ChangeMessage: app.Changed,
		},
		{
			Creator:       suite.addr2.String(),
			Timestamp:     app.Timestamp,
			TokenType:     "tokenType2",
			ChangeMessage: app.Changed,
		},
	}

	tokenId := suite.SetupToken()

	for _, element := range setup {
		token := suite.keeper.GetToken(suite.ctx, tokenId)
		suite.Require().True(token.Valid)
		token.Creator = element.Creator
		token.Timestamp = element.Timestamp
		token.TokenType = element.TokenType
		token.ChangeMessage = element.ChangeMessage
		tokenInfo := types.MsgUpdateTokenInformation{
			Creator: element.Creator,
			TokenId: tokenId,
			Data:    "data",
		}
		suite.keeper.SetTokenInformation(suite.ctx, tokenInfo)
		suite.Require().NotNil(suite.keeper.GetToken(suite.ctx, tokenId).Info)

		// set token
		suite.keeper.SetToken(suite.ctx, token)
		b := suite.keeper.HasTokenHistory(suite.ctx, tokenId)
		history := suite.keeper.GetAllTokenHistory(suite.ctx)
		_ = history
		suite.Require().True(b)

		// get token owner
		owner := suite.keeper.GetTokenOwner(suite.ctx, tokenId)
		suite.Require().Equal(element.Creator, owner)

		// set token invalid
		suite.keeper.SetFalse(suite.ctx, tokenId)
		suite.Require().False(suite.keeper.GetToken(suite.ctx, tokenId).Valid)
		suite.keeper.SetTrue(suite.ctx, tokenId)
		suite.Require().True(suite.keeper.GetToken(suite.ctx, tokenId).Valid)
	}
}

func (suite *TokenTestSuite) TestCreateTokenHistory() {
	addr := []string{suite.addr1.String(), suite.addr2.String()}
	setup := []types.MsgCreateTokenHistory{
		{
			Creator: suite.addr1.String(),
			Id:      app.TokenId1,
			History: []*types.Token{},
		},
		{
			Creator: suite.addr2.String(),
			Id:      app.TokenId2,
			History: []*types.Token{},
		},
	}

	for index, element := range setup {
		id := suite.keeper.AppendTokenHistory(suite.ctx, element)
		exists := suite.keeper.HasTokenHistory(suite.ctx, id)
		suite.Require().True(exists)

		out := suite.keeper.GetTokenHistory(suite.ctx, id)
		suite.Require().Equal(int(index)+1, len(suite.keeper.GetAllTokenHistory(suite.ctx)))
		suite.Require().Equal(addr[index], out.Creator)
		suite.Require().Equal(element.Id, out.Id)

		// get token history owner
		owner := suite.keeper.GetTokenHistoryOwner(suite.ctx, id)
		suite.Require().Equal(element.Creator, owner)
	}
}

func (suite *TokenTestSuite) TestSetTokenHistory() {
	// create token history entry
	id := suite.keeper.AppendTokenHistory(suite.ctx, types.MsgCreateTokenHistory{
		Creator: suite.addr1.String(),
		Id:      app.TokenId1,
		History: []*types.Token{},
	})

	out := suite.keeper.GetTokenHistory(suite.ctx, id)
	out.Creator = app.TokenCreator
	suite.keeper.SetTokenHistory(suite.ctx, out)
	out = suite.keeper.GetTokenHistory(suite.ctx, id)
	suite.Require().Equal(out.Creator, app.TokenCreator)
}
